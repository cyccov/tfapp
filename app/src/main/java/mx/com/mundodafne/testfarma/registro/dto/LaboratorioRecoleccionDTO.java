package mx.com.mundodafne.testfarma.registro.dto;

public class LaboratorioRecoleccionDTO {
    private String nombreLaboratorio;

    public String getNombreLaboratorio() {
        return nombreLaboratorio;
    }

    public void setNombreLaboratorio(String nombreLaboratorio) {
        this.nombreLaboratorio = nombreLaboratorio;
    }
}
