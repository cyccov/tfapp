package mx.com.mundodafne.testfarma.registro.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import mx.com.mundodafne.testfarma.registro.R;
import mx.com.mundodafne.testfarma.registro.app.ReporteExcelApp;

public class RegistroBaseActivity extends AppCompatActivity {
    //tutorial: https://www.youtube.com/watch?v=2m28IQz5LpY
    //repo: https://github.com/xcheko51x/ApachePOI-Android-Studio/blob/master/build.gradle
    //https://blog.aspose.com/cells/create-excel-file-in-android-programmatically/#:~:text=Create%20Excel%20XLSX%20or%20XLS%20in%20Android%23%201,workbook%20as%20an%20Excel%20file%20using%20the%20Workbook.save%28%29method.
    ReporteExcelApp reporteExcel = null;
    Button botonGenerarReporte;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_base3);
        reporteExcel = new ReporteExcelApp();

        botonGenerarReporte = findViewById(R.id.btn_registro_generar_reporte);
        botonGenerarReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reporteExcel.generarReporteExcel();
            }
        });
    }
}