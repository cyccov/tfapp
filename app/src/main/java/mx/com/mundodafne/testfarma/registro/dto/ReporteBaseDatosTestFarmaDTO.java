package mx.com.mundodafne.testfarma.registro.dto;

import java.util.Date;

public class ReporteBaseDatosTestFarmaDTO {
    private String auxiliarDeteccion;
    private String tipoEjercicio;
    private Date fechaEjercicio;
    private String tipoRecoleccion;
    private LaboratorioRecoleccionDTO laboratorioRecoleccionDTO;
    private String paqueteria;
    private String numeroGuia;
    private Date fechaRecoleccion;
    private String idMuestra;
    private String tipoDeEstudio;
    private Integer tuboDorado;
    private Integer tuboLila;
    private String comentarios;
    private String ciudad;
    private String estado;
    private String institucion;
    private String mesReporado;
    private Integer anioReportado;

    public String getAuxiliarDeteccion() {
        return auxiliarDeteccion;
    }

    public void setAuxiliarDeteccion(String auxiliarDeteccion) {
        this.auxiliarDeteccion = auxiliarDeteccion;
    }

    public String getTipoEjercicio() {
        return tipoEjercicio;
    }

    public void setTipoEjercicio(String tipoEjercicio) {
        this.tipoEjercicio = tipoEjercicio;
    }

    public Date getFechaEjercicio() {
        return fechaEjercicio;
    }

    public void setFechaEjercicio(Date fechaEjercicio) {
        this.fechaEjercicio = fechaEjercicio;
    }

    public String getTipoRecoleccion() {
        return tipoRecoleccion;
    }

    public void setTipoRecoleccion(String tipoRecoleccion) {
        this.tipoRecoleccion = tipoRecoleccion;
    }

    public LaboratorioRecoleccionDTO getLaboratorioRecoleccionDTO() {
        return laboratorioRecoleccionDTO;
    }

    public void setLaboratorioRecoleccionDTO(LaboratorioRecoleccionDTO laboratorioRecoleccionDTO) {
        this.laboratorioRecoleccionDTO = laboratorioRecoleccionDTO;
    }

    public String getPaqueteria() {
        return paqueteria;
    }

    public void setPaqueteria(String paqueteria) {
        this.paqueteria = paqueteria;
    }

    public String getNumeroGuia() {
        return numeroGuia;
    }

    public void setNumeroGuia(String numeroGuia) {
        this.numeroGuia = numeroGuia;
    }

    public Date getFechaRecoleccion() {
        return fechaRecoleccion;
    }

    public void setFechaRecoleccion(Date fechaRecoleccion) {
        this.fechaRecoleccion = fechaRecoleccion;
    }

    public String getIdMuestra() {
        return idMuestra;
    }

    public void setIdMuestra(String idMuestra) {
        this.idMuestra = idMuestra;
    }

    public String getTipoDeEstudio() {
        return tipoDeEstudio;
    }

    public void setTipoDeEstudio(String tipoDeEstudio) {
        this.tipoDeEstudio = tipoDeEstudio;
    }

    public Integer getTuboDorado() {
        return tuboDorado;
    }

    public void setTuboDorado(Integer tuboDorado) {
        this.tuboDorado = tuboDorado;
    }

    public Integer getTuboLila() {
        return tuboLila;
    }

    public void setTuboLila(Integer tuboLila) {
        this.tuboLila = tuboLila;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getMesReporado() {
        return mesReporado;
    }

    public void setMesReporado(String mesReporado) {
        this.mesReporado = mesReporado;
    }

    public Integer getAnioReportado() {
        return anioReportado;
    }

    public void setAnioReportado(Integer anioReportado) {
        this.anioReportado = anioReportado;
    }
}
