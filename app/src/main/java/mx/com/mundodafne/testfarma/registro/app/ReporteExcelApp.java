package mx.com.mundodafne.testfarma.registro.app;


import android.util.Log;

import com.aspose.cells.FileFormatType;
import com.aspose.cells.Workbook;

public class ReporteExcelApp {
    //https://www.youtube.com/watch?v=2m28IQz5LpY || https://blog.aspose.com/cells/create-excel-file-in-android-programmatically/#:~:text=Create%20Excel%20XLSX%20or%20XLS%20in%20Android%23%201,workbook%20as%20an%20Excel%20file%20using%20the%20Workbook.save%28%29method.
//https://jexcelapi.sourceforge.net/resources/javadocs/2_6_10/docs/index.html
    //https://www.vogella.com/tutorials/JavaExcel/article.html
    Workbook workbook = null;
    public void generarReporteExcel() {
        try {
            workbook = new Workbook();
            workbook.getWorksheets().get(0).getCells().get("A0").putValue("A1");
            workbook.save("/Android/data/mx.com.mundodafne.testfarma.registro.app/files/Excel.xls", FileFormatType.EXCEL_97_TO_2003);
        } catch (Exception e) {
            Log.e("e",e.getMessage());
        }
    }
}